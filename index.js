module.exports = {
  parserPreset: {
    parserOpts: {
      // referenceActions: [],
      issuePrefixes: ['DCF-']
    },
  },
  rules: {
		'body-leading-blank': [2, 'always'],
    'footer-leading-blank': [1, 'always'],
    'body-min-length': [2, 'always', 5],
		'header-max-length': [2, 'always', 72],
		'scope-case': [2, 'always', 'lower-case'],
		'subject-case': [
			2,
			'never',
			['sentence-case', 'start-case', 'pascal-case', 'upper-case']
		],
		'subject-empty': [2, 'never'],
		'subject-full-stop': [2, 'never', '.'],
		'type-case': [2, 'always', 'lower-case'],
		'type-empty': [2, 'never'],
		'type-enum': [
      2,
			'always',
			[
        'build',
				'chore',
				'ci',
				'docs',
				'feat',
				'fix',
				'refactor',
				'revert',
				'style',
				'test',
				'version'
			]
		],
    'scope-empty': [2, 'never'],
    'scope-enum': [
      2,
			'always',
			[
        'app',
        'api',
				'rpc',
				'ci',
				'env',
				'docs',
				'docker',
				'tests',
				'configs',
				'ecosystem'
			]
    ],
    'references-empty': [2, 'never']
	}
};
